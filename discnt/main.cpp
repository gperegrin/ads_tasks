#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>


struct INPUT_DATA {
    std::vector<int> prices;
    int discountRate;
};

struct OUTPUT_DATA {
    double minPurchaseSum;
};


struct INPUT_DATA readInput(std::string fileName) {
    std::ifstream inputFile(fileName, std::ifstream::in);

    struct INPUT_DATA inputData;
    inputData.prices = std::vector<int>();

    std::string priceLine;
    std::getline(inputFile, priceLine);
    std::istringstream priceLineStream(priceLine);

    int price;
    while (priceLineStream >> price)
    {
        inputData.prices.push_back(price);
    }

    inputFile >> inputData.discountRate;
    return inputData;
}

void merge(std::vector<int> &array, std::vector<int> &aux, int left, int middle, int right) {
    // Sedgewick's implementation of 'merge'.

    // Back up array items to 'aux'.
    for (int k = left; k <= right; k++) {
        aux[k] = array[k];
    }

    // Merge 'aux' back to 'array'.
    int i = left, j = middle + 1;
    for (int k = left; k <= right; k++) {
        if (i > middle)            array[k] = aux[j++];
        else if (j > right)        array[k] = aux[i++];
        else if (aux[j] > aux[i])  array[k] = aux[j++];
        else                       array[k] = aux[i++];
    }
}

void mergeSortBottomUp(std::vector<int> &array) {
    int n = (int)array.size();
    std::vector<int> aux(n);

    for (int stride = 1; stride < n; stride *= 2) {
        for (int i = 0; i < n - stride; i += stride * 2) {
            int left = i;
            int middle = i + stride - 1;
            int right = std::min(i + stride * 2 - 1, n - 1);
            merge(array, aux, left, middle, right);
        }
    }
}

struct OUTPUT_DATA solve(struct INPUT_DATA inputData) {
    auto prices = inputData.prices;
    auto discountRate = inputData.discountRate;

    // Sort the prices in descending order, apply the discount to first 1/3, calculate grand total.
    double minPurchaseSum = 0;
    auto discountedItemsCount = prices.size() / 3;

    mergeSortBottomUp(prices);
    for (int i = 0; i < prices.size(); i++) {
        minPurchaseSum += (i < discountedItemsCount) ? prices[i] * (1.0 - discountRate / 100.0) : prices[i];
    }

    struct OUTPUT_DATA outputData;
    outputData.minPurchaseSum = minPurchaseSum;

    return outputData;
}

void writeOutput(std::string fileName, struct OUTPUT_DATA outputData) {
    std::ofstream outputFile(fileName, std::ifstream::out);
    outputFile << std::setprecision(2) << std::fixed << outputData.minPurchaseSum;
    outputFile.close();
}

int main(int argc, const char *argv[]) {
    auto inputFileName = argc == 1 ? "discnt.in" : argv[1];
    auto outputFileName = argc == 1 ? "discnt.out" : argv[2];

    auto inputData = readInput(inputFileName);
    auto minPurchaseSum = solve(inputData);
    writeOutput(outputFileName, minPurchaseSum);
    return 0;
}
